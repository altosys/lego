#!/bin/bash
set -e
if [ "$VERBOSE" != "" ]; then
    set -x
fi

: "${DNS_PROVIDER:=dns}"
: "${DOMAINS:=example.com}"
: "${DO_PROPAGATION_TIMEOUT=15m}"
: "${EMAIL:=dummy@example.com}"
: "${KV_PATH:=kv/lego/data}"
: "${SERVER:=https://acme-v02.api.letsencrypt.org/directory}"

export DO_PROPAGATION_TIMEOUT
printf "%s\n\n" "$(lego --version)"

# mirror folder structure and data from vault to filesystem
fetch_kv_data() {
    vault kv list "$1" | tail -n +3 | grep -v healthcheck | while read -r path; do
        case "$path" in
        */)
            mkdir -p "$1/$path"
            fetch_kv_data "$1/$path"
            ;;
        *)
            vault kv get -field contents "$1$path" >"$1$path"
            ;;
        esac
    done
}

# parse comma separated domains into a series of cli flags
parse_domains() {
    domains=$(echo "$1" | tr "," "\n")
    for domain in "${domains[@]}"; do
        domain="$(echo "$domain" | tr -d '[:space:]')"
        domain_opts="$domain_opts --domains $domain"
    done
}

if [ "$VERBOSE" != "" ]; then
    echo "ACTION: $ACTION"
    echo "DNS_PROVIDER: $DNS_PROVIDER"
    echo "DOMAINS: $DOMAINS"
    echo "EMAIL: $EMAIL"
    echo "KV_PATH: $KV_PATH"
    echo "PING_ADDR: $PING_ADDR"
    echo "domain_opts: $domain_opts"
fi

printf "Parsing list of domains... "
parse_domains "$DOMAINS"
printf "done\n"

printf "Retrieving existing data from Vault... "
mkdir -p "$KV_PATH"
fetch_kv_data "$KV_PATH"
cp -r "$KV_PATH" pre-run
printf "done\n"

# shellcheck disable=SC2086
lego \
    --accept-tos \
    --dns "$DNS_PROVIDER" \
    --email "$EMAIL" \
    --path "$KV_PATH" \
    --server "$SERVER" \
    "$domain_opts" \
    "$ACTION"

if ! diff -rq pre-run "$KV_PATH"; then
    printf "Uploading new data to Vault...\n"
    find "$KV_PATH" -type f -print -exec vault kv put "{}" contents=@{} \;
fi

if [ "$PING_ADDR" != "" ]; then
    printf "Pinging health endpoint..."
    curl -fsS -m 10 --retry 5 -o /dev/null "$PING_ADDR"
    printf "done\n"
fi

echo Finished
