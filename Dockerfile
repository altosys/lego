FROM alpine:3
ARG OS=linux
ARG ARCH=amd64
ARG VERSION=4.10.2
RUN apk add -Uu --no-cache --no-progress \
  bash \
  curl \
  diffutils \
  findutils \
  libcap \
  ca-certificates \
  tzdata \
  vault && \
  update-ca-certificates
RUN setcap cap_ipc_lock= /usr/sbin/vault
RUN wget -O lego.tar.gz https://github.com/go-acme/lego/releases/download/v${VERSION}/lego_v${VERSION}_${OS}_${ARCH}.tar.gz && \
  tar -xvf lego.tar.gz lego --directory /usr/sbin/ && \
  rm lego.tar.gz

COPY lego.sh /lego.sh
RUN chmod 755 /lego.sh
ENTRYPOINT ["/lego.sh"]
